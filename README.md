[TOC]

# lib-fabric-dreamhost


## Bootstrap


### create_virtualenv

### install_bash_it

### configure_domain

### passenger_wsgi

### newrelic



## Deploy


### git_clone

### git_update(project_name, branch='master')

### pip_install

### collectstatic

### db_migrate

### restart
